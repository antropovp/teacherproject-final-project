// Written by Pavel Antropov (me)

$(document).ready(function() {
	$('input[type="submit"]').click(function() {
		$('.fill input[required=""]').each(function() {
			var input = $(this);
			if (input.val().length <= 0) {
				$(input).css('box-shadow', '0 0 7px 1px #f00');
				$(input).animate({ 
				boxShadow: "0 0 0 0 #fff"
			}, 1500, 'easeInCubic');
			}
		});
	});
});