<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Restore password</title>
	<link rel="icon" type="image/ico" sizes="128x128" href="img/favicon.ico">

	<script type="text/javascript" src="js/jquery.check-input.js"></script>
	<script type="text/javascript" src="js/jquery.animate-shadow-min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style_forms.css">

	<!-- Похоже, не получается сделать общий стиль для форм. Тот, что в файле style_forms выглядит уродливо при открытии в небольшом окне - расползается -->
	<!-- Либо нужно просто переписать общий -->
	<style type="text/css">
		#title {
			line-height: 1.5em; font-size: 1.6em;
			cursor: default;
		}
		form {
			position: absolute;
			top: 30%;
		}
		.fill > div {
			width: 100%;
			margin-bottom: 5%;
		}
		#btn_submit {
			width: 100%;
		}
	</style>
</head>
<body>
	<div id="header">
		<span id="title">
			Teacher
		</span>
	</div>

	<form name="restore_password" action="restore_password" method="post" id="restorePasswordForm">
		<div class="fill">
			<div>
				<input required="" type="text" name="username" maxlength="16" placeholder="username">
			</div>
		</div>
		<input id="btn_submit" type="submit" value="Remind password">
	</form>

</body>
</html>