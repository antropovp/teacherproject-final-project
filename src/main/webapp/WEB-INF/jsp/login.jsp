<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>TeacherProject</title>
	<link rel="icon" type="image/ico" sizes="128x128" href="img/favicon.ico">

	<link rel="stylesheet" type="text/css" href="css/style_index.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">

	<script type="text/javascript" src="js/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="js/jquery.check-input.js"></script>
	<script type="text/javascript" src="js/jquery.animate-shadow-min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
</head>
<body>

	<div id="main">
		<div id="title">
			<span>Teacher</span>
		</div>

		<div id="sign_in">
			<form name="sign_in" action="login" method="post" id="loginForm">
				<div class="fill">
					<div>
						<input type="text" name="username" required="" placeholder="username" maxlength="16" size="30%">
					</div>
					<div>
						<input type="password" name="password" required="" placeholder="password" maxlength="16" size="30%">
					</div>
				</div>
				<input id="btn_sign_in" type="submit" value="Sign In">
			</form>
			<div id="links">
				<div><a href="#" onclick="window.open('restore_password', 'restorePass', 'left=408, top=150, width=550, height=350')">Forgot password?</a></div>
				<div><a href="#" onclick="window.open('registration')">Registration</a></div>
			</div>
		</div>

		<div id="media">
			<div id="media_button">
				<img src="img/facebook.png" width="48" height="48" onclick="window.location='https://www.facebook.com'">
				<img src="img/plus.png" width="48" height="48" onclick="window.location='https://plus.google.com'">
				<img src="img/twitter.png" width="48" height="48" onclick="window.location='https://www.twitter.com'">
			</div>
		</div>
	</div>

	<div id="footer">
		<span>
			Created by Pavel Antropov &#169 2018
			<br>
			All rights reserved.
		</span>
	</div>
	
</body>
</html>