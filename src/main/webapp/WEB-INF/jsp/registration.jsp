<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Registration</title>
	<link rel="icon" type="image/ico" sizes="128x128" href="img/favicon.ico">

	<link rel="stylesheet" type="text/css" href="css/style_forms.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">

	<script type="text/javascript" src="js/jquery-3.2.1.js"></script>
	<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<script type="text/javascript" src="js/jquery.check-input.js"></script>
	<script type="text/javascript" src="js/jquery.animate-shadow-min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>

	<style type="text/css">
		.fill > div, .role {
			display: inline-block;
			margin-right: 2px;
			margin-left: 2px;
		}

		.role {
			width: 270px; height: 100px;

			font-size: 22pt;
			line-height: 1.75em;

			cursor: pointer;
		}

		.role div {
			margin-top: 8%;
		}

		.role ::selection {
			background: transparent;
		}

		input[type="radio"] {
			display: none;
		}

		#student {
			box-shadow: 0 0 5px #000;
		}

		#student {
			background-color: #fff;
			border: 2px solid #083;
			color: #083;
		}

		#teacher {
			background-color: #083;
			color: #fff;
		}

		#agree {
			font-family: "Arial", sans-serif;
		}
	</style>
</head>
<body>
	<div id="header">
		<span id="title" onclick="window.location='login';">
			Teacher
		</span>
	</div>

	<form name="registration" action="registration" method="post" id="registrationForm">
		<div class="fill">
			<div style="margin-bottom: 4%;">
				<input required="" type="text" name="username" maxlength="16" placeholder="username">
			</div>
			<div>
				<input required="" type="password" name="password" maxlength="16" placeholder="password">
			</div>

			<div class="name">
				<input type="text" name="firstName" maxlength="24" placeholder="First Name">
			</div>
			<div class="name">
				<input type="text" name="lastName" maxlength="24" placeholder="Last Name">
			</div>
		</div>

		<div id="role">
			<div class="role" id="student">
				<div>
					I'm a Student
					<input required="" type="radio" name="role" value="STUDENT" checked="">
				</div>
			</div>
			<div class="role" id="teacher">
				<div>
					I'm a Teacher
					<input required="" type="radio" name="role" value="TEACHER">
				</div>
			</div>
		</div>

		<div class="g-recaptcha" data-sitekey="6LcSoU4UAAAAAAVL8g8fAp1k5mTs1RNZ7BOO5wQE" style="margin-top: -2%;"></div>

		<div id="agree">
			<input required="" type="checkbox" name="agree">I agree with the <a href="#" onclick="window.open('')">terms and conditions</a>.
		</div>

		<div style="margin-bottom: 0;">
			<input id="btn_submit" type="submit" value="Sign Up">
		</div>
	</form>

	<script type="text/javascript">
		$(document).ready(function() {
			$('.role').click(function() {
				$(this).find('input').prop('checked', true);
				$(this).css('box-shadow', '0 0 5px #000');
				$(this).siblings().css('box-shadow', 'none');
			});
		});
	</script>
</body>
</html>