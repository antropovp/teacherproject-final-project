<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>TeacherProject</title>
	<link rel="icon" type="image/ico" sizes="128x128" href="img/favicon.ico">

	<link rel="stylesheet" type="text/css" href="css/style_main.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">

	<script type="text/javascript" src="js/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="js/jquery.animate-shadow-min.js"></script>
	<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
</head>
<body>
	<div id="content">
		<!-- Content -->
	</div>

	<!-- Test creating pop-up (basic info) -->
	<div id="create_test_popup" hidden="">
		<div class="overlay"></div>
		<div class="popup_window">
			<div class="popup_close" onclick="$('#create_test_popup').prop('hidden', true);">x</div>
			<form id="basic_form" class="popup_inner">
				<div>
					<input required="" type="text" name="name" placeholder="Test name" maxlength="24">
				</div>
				<div>
					<input required="" type="text" name="subject" autocomplete="" selectBoxOptions="Math;Physics;Chemistry;History" placeholder="Subject" maxlength="24">
				</div>
				<input class="button" type="submit" id="basic_form_btn" value="Add questions" url="create">
			</form>
		</div>
	</div>

	<!-- Page sceleton -->
	<div id="header">
		<div id="title" onclick="window.location='/';">
			<span>Teacher</span>
		</div>
		<div id="exit">
			<a href="logout"><img src="img/exit_icon.svg"></a>
		</div>		
	</div>
	<div id="leftbar">
	    <c:if test="${user.role == 'TEACHER'}">
		    <div class="button" onclick="$('#create_test_popup').prop('hidden', false);">Create a test</div>
		</c:if>
		<div class="button" url="pages/t/testing">Open demo test</div>
	</div>
	<div id="rightbar">
		<div class="button" url="pages/profile" style="font-size: 13pt;">Profile</div>
		<div class="button" url="pages/users">Users</div>
		<div class="button" url="pages/tests">Tests</div>
	</div>
	<div id="footer">
		<div id="footer_text">
			<span>
				Created by Pavel Antropov &#169 2018
				<br>
				All rights reserved.
			</span>
		</div>
	</div>

	<script type="text/javascript">
		$('#content').load('pages/t/tests');

		// Передаются на страницу создания теста
		var test_name;
		var subject_name;
		// Проверка, создаётся просто новый вопрос или новый тест (для работы кнопки Add Question на странице создания теста)
		var isNewQ = false;

		$('.button').click(function() {
		    var url = this.getAttribute('url');
		    $('#content').load(url);
		});

		$('#basic_form_btn').click(function() {
			test_name = $('#create_test_popup input[name="name"]').val();
			subject_name = $('#create_test_popup input[name="subject"]').val();

			var correct = true;
			$('#create_test_popup input[type="text"]').each(function() {
				var input = $(this);
				
				if (input.val().length <= 0) {
					correct = false;
					$(input).css('box-shadow', '0 0 7px 1px #f00');
					$(input).animate({ 
					boxShadow: "0 0 0 0 #fff"
				}, 1500, 'easeInCubic');
				}
			});
			if (correct) {
				isNewQ = false;
				sendAjaxForm('basic_form', 'create');
				$('#create_test_popup').prop('hidden', true);
			}
		});

		function sendAjaxForm(form, url) {
			$.ajax({
				url: url,
				type: "post",
				data:  form.serialize(),
				success: function() {
					alert("ok");
					$('#content').load('pages/t/create');
				},
				error: function() {
					alert("Error while sending the form.");
				}
			})
		}
	</script>
</body>
</html>