<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Users</title>

	<link rel="stylesheet" type="text/css" href="../css/style_tests.css">
</head>
<body>
	<form id="search_tool" method="get">
		<div id="st_inner">
			<div id="search_button">
				<input type="button" name="search_submit" value="">
			</div>
			<div id="underline">
				<input required="" type="text" name="search" placeholder="Search...">
			</div>
		</div>
	</form>

	<div id="search_result" hidden="">
		<div class="test_list">
			<div class="list_head">
				<span>Results:</span>
			</div>
			<div class="container"></div>
		</div>
		<!-- По js сохранять все совпадения в массив и выводить сюда -->
	</div>

	<!-- Popup when trying to delete test -->
	<div id="delete_popup" hidden="">
		<div class="overlay"></div>
		<div class="popup_window">
			<div class="popup_close" onclick="$('#delete_popup').prop('hidden', true);">x</div>
			<div class="popup_inner">
				<div><span>Are you sure?</span></div>
				<button class="button" type="button" onclick="$('#delete_popup').prop('hidden', true); $(test).remove();">Yes</button>
				<button class="button" type="button" onclick="$('#delete_popup').prop('hidden', true);">Cancel</button>
			</div>
		</div>
	</div>
	
	<div id="tests">
	    <c:if test="${user.role == 'TEACHER'}">
            <div class="test_list" id="my_tests">
                <div class="list_head">
                    <span>My tests:</span>
                </div>
                <div class="container">
                    <div class="test" id="add_test" onclick="$('#create_test_popup').prop('hidden', false);">
                        <div class="cover">
                            <img src="../img/plus.svg" width="54" height="54" style="margin-top: 39px;">
                        </div>
                    </div>
                    <div class="test"></div>
                    <div class="test"></div>
                </div>
            </div>
		</c:if>
		<div class="test_list">
			<div class="list_head">
				<span>Top tests:</span>
			</div>
			<div class="container">
				<div class="test"></div>
				<div class="test"></div>
				<div class="test"></div>
				<div class="test"></div>				
			</div>
		</div>
		<div class="test_list">
			<div class="list_head">
				<span>Math:</span>
			</div>
			<div class="container">
				<div class="test"></div>
				<div class="test"></div>
				<div class="test"></div>				
			</div>
		</div>
		<div class="test_list">
			<div class="list_head">
				<span>Physics:</span>
			</div>
			<div class="container">
				<div class="test"></div>
				<div class="test"></div>
				<div class="test"></div>
				<div class="test"></div>
				<div class="test"></div>				
			</div>
		</div>
		<div class="test_list">
			<div class="list_head">
				<span>History:</span>
			</div>
			<div class="container">
				<div class="test"></div>
				<div class="test"></div>				
			</div>
		</div>
	</div>
	
	<div style="height: 100px;"></div>

	<script type="text/javascript">
		var test = null;

		$('.container').prop('align', 'left');
		$('.test:not(#add_test)').append('<div class="cover"><div class="cover_image"></div><div class="cover_name"><span>name</span></div></div><div class="options"></div>');
		// иконки
		$('.options').html('<img class="change_test" src="../img/pencil.svg" width="16" height="16"><img class="delete_test" src="../img/delete_icon.svg" width="16" height="16">');
		$('.options').prop('align', 'right');

		$(document).ready(function() {
			$('.change_test').click(function() {
				test_name = $(this).parent().parent().find('.cover_name span').text();
				subject_name = $(this).parent().parent().find('.subject_ico span').text();
				$('#content').load('../pages/t/change');
			});
			$('.delete_test').click(function() {
				test = $(this).parent().parent();
				$('#delete_popup').prop('hidden', false);
			});
			$('input[type="button"]').click(function() {
				var input = $('#search_tool input[type="text"]');
				if (input.val().length > 0) {
					$('#tests').prop('hidden', true);
					$('#search_result').prop('hidden', false);
				}
				else {
					$(input).css('box-shadow', '0 0 7px 1px #f00');
				}
				$(input).animate({ 
					boxShadow: "0 0 0 0 #fff"
				}, 1500, 'easeInCubic');
			});
		});
	</script>
</body>
</html>