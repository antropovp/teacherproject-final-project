<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Users</title>

	<link rel="stylesheet" type="text/css" href="../css/style_search.css">
</head>
<body>
	<div>
		Best users:
	</div>
	<div id="search">
		<form name="search_tool" action="#">
			<div>
				<input type="text" name="username" placeholder="username">
			</div>
			<div>
				<select name="role">
					<option disabled="" selected="">Role</option>
					<option value="student">Student</option>
					<option value="teacher">Teacher</option>
					<option value="moderator">Moderator</option>
					<option value="admin">Admin</option>
				</select>
			</div>
			<div>
				<!-- Это лучше реализовать в виде стрелочек ▼▲ под названием каждого столбца -->
				<select name="sortby">
					<option disabled="" selected="">Sort by</option>
					<option value="username">Username</option>
					<option value="name">Name</option>
					<option value="role">Role</option>
					<option value="rating">Rating</option>
				</select>
			</div>
		</form>
		<div id="table">
			<div class="column">
				<div class="column_name" style="border-left: 2px solid #083;">Username</div>
				<div class="results">info</div>
			</div>
			<div class="column">
				<div class="column_name">Name</div>
				<div class="results">info</div>
			</div>
			<div class="column">
				<div class="column_name">Role</div>
				<div class="results">info</div>
			</div>
			<div class="column">
				<div class="column_name" style="border-right: 2px solid #083;">Rating</div>
				<div class="results">info</div>
			</div>
		</div>
	</div>
</body>
</html>