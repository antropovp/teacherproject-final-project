<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Create test</title>

	<link rel="stylesheet" type="text/css" href="../../css/style_create.css">
</head>

	<style type="text/css">
		#q_navigation {
			float: right;

			height: 5%;
		}
		#q_navigation > div {
			float: left; vertical-align: middle;
			display: inline-block;
			margin-right: 5px;

			width: 24px; height: 24px;
			background-color: #083;

			color: #fff;

			cursor: pointer;
		}
		#q_navigation div::selection {
			background: transparent;
		}
		#q_navigation > div span {
			margin-right: 5px;
			
			font-size: 14pt;
		}
	</style>
<body>
	<form name="change_test">
		<div id="question">
			<div id="info">
				<span>Test name: <span id="test_name"></span></span>
				<span>Subject: <span id="subject_name"></span></span>
				<div id="q_navigation" align="right"></div>
			</div>
			<div id="isMulti">
				<div class="isMulti" style="margin-bottom: 2px;">
					<label class="container">
						<input required="" type="radio" name="isMulti" value="single" checked="">
						<span class="checkmark"></span>
					</label>
					<div>Single answer</div>
				</div>
				<div class="isMulti">
					<label class="container">
						<input type="radio" name="isMulti" value="multi">
						<span class="checkmark"></span>
					</label>
					<div>Multi answer</div>
				</div>
			</div>

			<div id="points">
				<span style="font-weight: bold; font-size: 15pt;">Points: </span>
				<img id="down" src="../../img/arrow_left.svg">
				<input required="" type="text" name="points" placeholder="0" maxlength="2" value="1">
				<img id="up" src="../../img/arrow_right.svg">
			</div>

			<div id="text">
				<textarea required="" name="text" placeholder="Write the question text here..." rows="4"></textarea>
			</div>
			<div id="answers" align="left">
				<div style="display: inline-block;"></div>
				<button type="button" name="add_answer">+</button>
				<button type="button" name="remove_answer">-</button>
			</div>
		</div>

		<input class="big_btn" type="button" name="add_question" value="Add answer">
		<input class="big_btn" type="submit" name="create" value="Save test">
	</form>

	<script type="text/javascript">
		$('#test_name').text(test_name);
		$('#subject_name').text(subject_name);

		var questions_num = 5;
		for (var i = 1; i <= questions_num; i++) {
			$('#q_navigation').append('<div><span>'+i+'</span></div>');
		}
		$('#q_navigation div').first().css({'border':'1px solid #083', 'background-color':'#fff', 'color':'#083'});

		var answers_num = 4;
		for (var i = 0; i < answers_num; i++) {
			$('#answers > div').append('<input required="" type="text" name="answer" placeholder="Possible answer">');
		}

		function checkInput() {
			var correct = true;

			var text = $('#text textarea');
			if (text.val().length <= 0) {
				correct = false;
				$(text).css('box-shadow', '0 0 7px 1px #f00');
				$(text).animate({ 
					boxShadow: "0 0 0 0 #fff"
				}, 1500, 'easeInCubic');
			}
			$('#answers input[type="text"]').each(function() {
				var input = $(this);
				
				if (input.val().length <= 0) {
					correct = false;
					$(input).css('box-shadow', '0 0 7px 1px #f00');
					$(input).animate({ 
					boxShadow: "0 0 0 0 #fff"
				}, 1500, 'easeInCubic');
				}
			});

			return correct;
		}
		
		$(document).ready(function() {
			$('#q_navigation div').click(function() {
				$('#q_navigation div').css({'border':'none', 'background-color':'#083', 'color':'#fff'});
				$(this).css({'border':'1px solid #083', 'background-color':'#fff', 'color':'#083'});
			});

			$('.isMulti').click(function() {
				$(this).find('input').prop('checked', true);
			});
			$('#down').click(function() {
				var value = $(this).siblings('input').val();
				if ($.isNumeric(value) && value > 0) {
					$(this).siblings('input').val(+value-1);
				}
				else {
					$(this).siblings('input').val('0');
				}
			});
			$('#up').click(function() {
				var value = $(this).siblings('input').val();
				if ($.isNumeric(value)) {
					$(this).siblings('input').val(+value+1);
				}
				else {
					$(this).siblings('input').val('1');
				}
			});
			$('button[name="add_answer"]').click(function() {
				answers_num++;
				if (answers_num == 3) {
					$('button[name="add_answer"]').css({'position':'absolute'});
					$('button[name="remove_answer"]').prop('hidden', false);
				}
				if (answers_num <= 12) {
					$('#answers > div').append('<input required="" type="text" name="answer" placeholder="Possible answer">');
				}
			});
			$('button[name="remove_answer"]').click(function() {
				answers_num--;
				if (answers_num >= 2) {
					$('#answers > div input').last().remove();
				}
				if (answers_num == 2) {
					$('button[name="remove_answer"]').prop('hidden', true);
					$('button[name="add_answer"]').css({'position':'static'});
				}
			});
			$('.big_btn[name="add_question"]').click(function () {
				if (checkInput()) {
					//По введенным данным создаём вопрос, дальше:
					isNewQ = true;
					$('#content').load('pages/t/create');
					$('#q_num').text(q_num++);
				}
			});
		});
	</script>
</body>
</html>