<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Create test</title>

	<style type="text/css">
		#question_text {
			margin-top: 3%; margin-left: 10%;
			width: 80%;
			font-size: 16pt;
			font-weight: bold;
		}

		#answers {
			margin-left: 33.6%; margin-top: 3.3%;

			font-size: 14pt;
		}
		#answers .answer {
			float: left;
			display: inline-block;
			width: 140px; height: 25px;
			margin-right: 4%; margin-bottom: 1%;

			text-align: left;

			border-bottom: 2px solid #083;
			border-radius: 2px;
		}
		#answers .answer span {
			margin-left: 3px;
		}

		#next_prev {
			position: absolute;
			top: 60%;
			width: 100%;
		}
		form[name="testing"] button {
			border: none;
			width: 220px; height: 50px;
			background-color: #083;

			font-size: 24pt;
			color: #fff;

			cursor: pointer;
		}

		#q_navigation {
			width: 100%; height: 100%;
			position: absolute;
			top: 4.5%; left: 0;
			display: flex;
			align-items: center;
			align-content: center;
			justify-content: center;
			overflow: auto;

			height: 5%;
		}
		#q_navigation > div {
			float: left; vertical-align: middle;
			display: inline-block;
			margin-right: 5px;

			width: 24px; height: 24px;
			background-color: #083;

			color: #fff;

			cursor: pointer;
		}
		#q_navigation div::selection {
			background: transparent;
		}
		#q_navigation > div span {
			vertical-align: middle;

			font-size: 13pt; 
		}
	</style>
</head>
<body>
	<div id="q_navigation"></div>

	<form name="testing">
		<div id="question_text">Some text that I wrote just now. I think it'll make a great question, won't it? Or maybe at least a placeholder, right?</div>

		<div id="answers"></div>

		<div id="next_prev">
			<button style="float: left;" type="button" name="prev">Prev question</button>
			<button style="float: right;" type="button" name="next">Next question</button>
		</div>
	</form>

	<script type="text/javascript">
		//По значению из базы

		var isMulti = false;
		var questions_num = 6;
		var answers_num = 5;
		var answers_in_a_row = 2;

		for (var i = 1; i <= questions_num; i++) {
			$('#q_navigation').append('<div><span>'+i+'</span></div>');
		}
		$('#q_navigation div').first().css({'border':'1px solid #083', 'background-color':'#fff', 'color':'#083'});

		if (answers_num > 6) {
			answers_in_a_row = 4;
			$('#answers').css('margin-left', '16%');
		}
		else if (answers_num > 4) {
			answers_in_a_row = 3;
			$('#answers').css('margin-left', '25%');
		}
		for (var i = 1; i <= answers_num; i++) {
			$('#answers').append('<div class="answer"><input type="" name="answer" value="?"><span>?</span></div>');
			if (i%answers_in_a_row == 0) {
				$('#answers').append('<br><br>');
			}
		}		
		
		if (isMulti) {
			$('#answers input').prop('type', 'checkbox');
		}
		else {
			$('#answers input').prop('type', 'radio');
		}

		$(document).ready(function() {
			$('#q_navigation div').click(function() {
				$('#q_navigation div').css({'border':'none', 'background-color':'#083', 'color':'#fff'});
				$(this).css({'border':'1px solid #083', 'background-color':'#fff', 'color':'#083'});
			});
		});
	</script>
</body>
</html>