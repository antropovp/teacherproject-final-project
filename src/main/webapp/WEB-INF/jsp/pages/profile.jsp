<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Profile</title> <!--Of any user-->

	<link rel="stylesheet" type="text/css" href="../css/style_main.css">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:900" rel="stylesheet">
</head>
<body>
	<div id="content">
		<b>${user.username}</b>, we're sorry, but we don't have profiles on our site.
	</div>
</body>
</html>