package com.antropovp.tp.enums;

public enum Role {
    STUDENT, TEACHER, MODERATOR, ADMIN
}
