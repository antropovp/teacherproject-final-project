package com.antropovp.tp.interceptor;

import com.antropovp.tp.dto.User;
import com.antropovp.tp.service.SessionUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    private SessionUserManager sessionUserManager;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        User user = sessionUserManager.getCurrentSessionUser();

        if (Objects.isNull(user)) {
            response.sendRedirect("login");
            return false;
        }

        return true;
    }
}
