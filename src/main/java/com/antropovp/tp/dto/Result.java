package com.antropovp.tp.dto;

import com.antropovp.tp.service.QuestionService;
import com.antropovp.tp.service.impl.QuestionServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class Result {

    public Result() {

    }
    public Result(long studentId, long testId) {
        this.studentId = studentId;
        this.testId = testId;
    }

    QuestionService questionService = new QuestionServiceImpl();
    private long id;
    private long studentId;
    private long testId;

    private List<Answer> studentAnswers = new ArrayList<>(); //данные студентом ответы

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    public List<Answer> getStudentAnswers() {
        return studentAnswers;
    }

    public void setStudentAnswers(List<Answer> studentAnswers) {
        this.studentAnswers = studentAnswers;
    }

    public int getNumberOfCorrectAnswers() {
        int number = 0;
        for (Answer answer : this.studentAnswers) {
            if (answer.isRight()) {
                number++;
            }
        }
        return number;
    }

    public int getSumOfPoints() {
        int sum = 0;
        for (Answer answer : this.studentAnswers) {
            if (answer.isRight()) {
                sum += questionService.get(answer.getQuestionId()).getPoints();
            }
        }
        return sum;
    }

    public char getGrade() {
        if ((float)getNumberOfCorrectAnswers() / studentAnswers.size() > 0.9) {
            return 'A';
        }
        else if ((float)getNumberOfCorrectAnswers() / studentAnswers.size() > 0.8) {
            return 'B';
        }
        else if ((float)getNumberOfCorrectAnswers() / studentAnswers.size() > 0.7) {
            return 'C';
        }
        else if ((float)getNumberOfCorrectAnswers() / studentAnswers.size() > 0.6) {
            return 'D';
        }
        else {
            return 'F';
        }
    }
}
