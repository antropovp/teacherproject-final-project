package com.antropovp.tp.dto;

import com.antropovp.tp.enums.Role;

import java.util.ArrayList;
import java.util.List;

public class User {

    private long id;
    private String username;
    private String password;
    private Role role;

    private String firstName;
    private String lastName;

    //Student
    private List<Result> studentResults = new ArrayList<>();

    //Teacher
    private List<Test> teacherTests = new ArrayList<>();

    public User() {

    }
    public User(String username, String password, Role role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }
    public User(String username, String password, Role role, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.role = role;

        this.firstName = firstName;
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    //By roles

    public List<Result> getStudentResults() {
        return studentResults;
    }

    public void setStudentResults(List<Result> studentResults) {
        this.studentResults = studentResults;
    }

    public List<Test> getTeacherTests() {
        return teacherTests;
    }

    public void setTeacherTests(List<Test> teacherTests) {
        this.teacherTests = teacherTests;
    }
}
