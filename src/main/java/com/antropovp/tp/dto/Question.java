package com.antropovp.tp.dto;

import java.util.ArrayList;
import java.util.List;

public class Question {

    private long id;
    private long testId;
    private boolean isMultiAnswer;
    private String text; //текст вопроса
    private int points; //баллы за верный ответ

    private List<Answer> answers = new ArrayList<>(); //список вариантов ответа

    public Question() {

    }
    public Question(long testId, String text, boolean isMultiAnswer, int points) {
        this.testId = testId;
        this.text = text;
        this.isMultiAnswer = isMultiAnswer;
        this.points = points;
    }
    public Question(long testId, String text, boolean isMultiAnswer, int points, List<Answer> answers) {
        this.testId = testId;
        this.text = text;
        this.isMultiAnswer = isMultiAnswer;
        this.points = points;
        this.answers = answers;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    public boolean isMultiAnswer() {
        return isMultiAnswer;
    }

    public void setMultiAnswer(boolean isMultiAnswer) {
        this.isMultiAnswer = isMultiAnswer;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", testId=" + testId +
                ", isMultiAnswer=" + isMultiAnswer +
                ", text='" + text + '\'' +
                ", points=" + points +
                ", answers=" + answers +
                '}';
    }
}
