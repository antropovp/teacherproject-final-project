package com.antropovp.tp.dto;

public class Answer {

    private long id;
    private long questionId;
    private String text; //текст варианта ответа
    private boolean isRight; //правильный-неправильный

    public Answer() {

    }
    public Answer(long questionId, String text, boolean isRight){
        this.questionId = questionId;
        this.text = text;
        this.isRight = isRight;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuestionId() {
        return getQuestionId();
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isRight() {
        return isRight;
    }

    public void setRight(boolean isRight) {
        this.isRight = isRight;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "id=" + id +
                ", questionId=" + questionId +
                ", text='" + text + '\'' +
                ", isRight=" + isRight +
                '}';
    }
}
