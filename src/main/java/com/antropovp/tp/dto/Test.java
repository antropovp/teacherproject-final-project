package com.antropovp.tp.dto;

import java.util.ArrayList;
import java.util.List;

public class Test {

    private long id;
    private String name;
    private String subject;
    private long teacherId; //id создавшего тест учителя
    private List<Question> questions = new ArrayList<>(); //вопросы
    private List<Result> results = new ArrayList<>(); //все результаты по этому тесту

    public Test() {
    }
    public Test(String name, String subject) {
        this.name = name;
        this.subject = subject;
    }
    public Test(long teacherId, String name, String subject) {
        this.teacherId = teacherId;
        this.name = name;
        this.subject = subject;
    }
    public Test(long teacherId, String name, String subject, List<Question> questions) {
        this.teacherId = teacherId;
        this.name = name;
        this.subject = subject;
        this.questions = questions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(long teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", subject=" + subject +
                ", teacherId=" + teacherId +
                ", questions=" + questions +
                ", results=" + results +
                '}';
    }
}
