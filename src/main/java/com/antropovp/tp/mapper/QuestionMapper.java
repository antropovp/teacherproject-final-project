package com.antropovp.tp.mapper;

import com.antropovp.tp.dto.Question;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class QuestionMapper implements RowMapper<Question> {

    @Override
    public Question mapRow(ResultSet rs, int rowNum) throws SQLException {
        Question question = new Question();
        question.setId(rs.getInt("id"));
        question.setTestId(rs.getInt("testId"));
        question.setMultiAnswer(rs.getBoolean("isMultiAnswer"));
        question.setText(rs.getString("text"));
        question.setPoints(rs.getInt("points"));
        return question;
    }

}
