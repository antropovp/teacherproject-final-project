package com.antropovp.tp.mapper;

import com.antropovp.tp.dto.Result;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ResultMapper implements RowMapper<Result> {

    @Override
    public Result mapRow(ResultSet rs, int rowNum) throws SQLException {
        Result result = new Result();
        result.setId(rs.getInt("id"));
        result.setTestId(rs.getInt("testId"));
        result.setStudentId(rs.getInt("studentId"));
        return result;
    }

}
