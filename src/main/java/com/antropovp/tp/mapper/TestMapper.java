package com.antropovp.tp.mapper;

import com.antropovp.tp.dto.Test;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TestMapper implements RowMapper<Test> {

    @Override
    public Test mapRow(ResultSet rs, int rowNum) throws SQLException {
        Test test = new Test();
        test.setId(rs.getInt("id"));
        test.setName(rs.getString("name"));
        test.setSubject(rs.getString("subject"));
        test.setTeacherId(rs.getInt("teacherId"));
        //TODO: setQuestions() and setResults()
        return test;
    }
}