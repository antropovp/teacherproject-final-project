package com.antropovp.tp.mapper;

import com.antropovp.tp.dto.Answer;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AnswerMapper implements RowMapper<Answer> {

    @Override
    public Answer mapRow(ResultSet rs, int rowNum) throws SQLException {
        Answer answer = new Answer();
        answer.setId(rs.getInt("id"));
        answer.setQuestionId(rs.getInt("questionId"));
        answer.setText(rs.getString("text"));
        answer.setRight(rs.getBoolean("isRight"));
        return answer;
    }
}