package com.antropovp.tp.dao;

import com.antropovp.tp.dto.Question;
import com.antropovp.tp.dto.Test;

import java.util.List;

public interface TestDao {

    long add(Test test);
    void update(Test test);
    void remove(long id);

    Test get(long id);
    List<Test> getAllTests();
}
