package com.antropovp.tp.dao;

import com.antropovp.tp.dto.Answer;
import com.antropovp.tp.dto.Question;

import java.util.List;

public interface QuestionDao {

    long add(Question question);
    void update(Question question);
    void remove(long id);

    Question get(long id);
    List<Question> getQuestionsByTestId(long testId);
    List<Question> getAllQuestions();
}
