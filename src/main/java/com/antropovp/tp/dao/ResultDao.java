package com.antropovp.tp.dao;

import com.antropovp.tp.dto.Result;

import java.util.List;

public interface ResultDao {

    long add(Result result);
    void update(Result result);
    void remove(long id);

    Result get(long id);
    List<Result> getAllResults();
}
