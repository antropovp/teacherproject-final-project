package com.antropovp.tp.dao.impl;

import com.antropovp.tp.dto.Result;
import com.antropovp.tp.mapper.ResultMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class ResultDaoImpl implements com.antropovp.tp.dao.ResultDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public long add(Result result) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("sumOfPoints", result.getSumOfPoints())
                .addValue("testId", result.getTestId())
                .addValue("studentId", result.getStudentId());
        namedParameterJdbcTemplate.update("INSERT INTO `database`.result (sumOfPoints, testId, studentId) VALUES (:sumOfPoints, :testId, :studentId)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(Result result) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("UPDATE `database`.result SET sumOfPoints = ?, testId = ?, studentId = ? WHERE id = ?",
                new Object[]{result.getSumOfPoints(), result.getTestId(), result.getStudentId(), result.getId()});
    }

    @Override
    public void remove(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM `database`.result WHERE id = ?", id);
    }

    @Override
    public Result get(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM `database`.result WHERE id = ?", new Object[]{id}, new ResultMapper());
    }

    @Override
    public List<Result> getAllResults() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.result", new ResultMapper());
    }

}
