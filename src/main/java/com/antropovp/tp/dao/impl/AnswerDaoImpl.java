package com.antropovp.tp.dao.impl;

import com.antropovp.tp.dao.AnswerDao;
import com.antropovp.tp.dto.Answer;
import com.antropovp.tp.mapper.AnswerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class AnswerDaoImpl implements AnswerDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public long add(Answer answer) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("text", answer.getText())
                .addValue("isRight", answer.isRight())
                .addValue("questionId", answer.getQuestionId());
        namedParameterJdbcTemplate.update("INSERT INTO `database`.answer (text, isRight, questionId) VALUES (:text, :isRight, :questionId)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(Answer answer) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("UPDATE `database`.answer SET text = ?, isRight = ?, questionId = ? WHERE id = ?",
                new Object[]{answer.getText(), answer.isRight(), answer.getQuestionId(), answer.getId()});
    }

    @Override
    public void remove(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM `database`.answer WHERE id = ?", id);
    }

    @Override
    public Answer get(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM `database`.answer WHERE id = ?", new Object[]{id},
                new AnswerMapper());
    }

    @Override
    public List<Answer> getAnswersByQuestionId(long questionId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.answer WHERE question_id = ?", new Object[]{questionId},
                new AnswerMapper());
    }

    @Override
    public List<Answer> getAllAnswers() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.answer", new AnswerMapper());
    }
}
