package com.antropovp.tp.dao.impl;

import com.antropovp.tp.dto.Question;
import com.antropovp.tp.dto.Test;
import com.antropovp.tp.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class TestDaoImpl implements com.antropovp.tp.dao.TestDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public long add(Test test) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("name", test.getName())
                .addValue("subject", test.getSubject())
                .addValue("user_id", test.getTeacherId());
        namedParameterJdbcTemplate.update("INSERT INTO `database`.test (name, subject, user_id) VALUES (:name, :subject, :user_id)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(Test test) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("UPDATE `database`.test SET name = ?, subject = ? WHERE id = ?",
                new Object[]{test.getName(), test.getSubject(), test.getTeacherId(), test.getId()});
    }

    @Override
    public void remove(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM `database`.test WHERE id = ?", id);
    }

    @Override
    public Test get(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM `database`.test WHERE id = ?",
                new Object[]{id}, new TestMapper());
    }

    @Override
    public List<Test> getAllTests() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.test", new TestMapper());
    }
}
