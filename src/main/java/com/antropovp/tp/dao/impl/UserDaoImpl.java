package com.antropovp.tp.dao.impl;

import com.antropovp.tp.dao.UserDao;
import com.antropovp.tp.dto.User;
import com.antropovp.tp.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    DataSource dataSource;

    @Override
    public long add(User user) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("username", user.getUsername())
                .addValue("password", user.getPassword())
                .addValue("role", user.getRole().toString())
                .addValue("firstName", user.getFirstName())
                .addValue("lastName", user.getLastName());
        namedParameterJdbcTemplate.update("INSERT INTO `database`.user (username, password, role, firstName, lastName) " +
                "VALUES (:username, :password, :role, :firstName, :lastName)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(User user) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("UPDATE `database`.user SET username = ?, password = ?, role = ?, firstName = ?, lastName = ? WHERE id = ?",
                new Object[]{user.getUsername(), user.getPassword(), user.getRole(), user.getFirstName(), user.getLastName(), user.getId()});
    }

    @Override
    public void remove(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM `database`.user WHERE id = ?", id);
    }

    @Override
    public void remove(String username) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM `database`.user WHERE username = ?", username);
    }

    @Override
    public User get(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM `database`.user WHERE id = ?",
                new Object[]{id}, new UserMapper());
    }

    @Override
    public User get(String username) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM `database`.user WHERE username = ?",
                new Object[]{username}, new UserMapper());
    }

    @Override
    public List<User> getAllUsers() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.user",
                new UserMapper());
    }

    @Override
    public List<User> getAllTeachers() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.user WHERE role = 'TEACHER'",
                new UserMapper());
    }

    @Override
    public List<User> getAllStudents() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.user WHERE role = 'STUDENT'",
                new UserMapper());
    }
}
