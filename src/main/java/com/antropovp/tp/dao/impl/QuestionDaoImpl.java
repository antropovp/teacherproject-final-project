package com.antropovp.tp.dao.impl;

import com.antropovp.tp.dto.Answer;
import com.antropovp.tp.dto.Question;
import com.antropovp.tp.mapper.QuestionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class QuestionDaoImpl implements com.antropovp.tp.dao.QuestionDao {

    @Autowired
    private DataSource dataSource;

    @Override
    public long add(Question question) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
                .addValue("isMultiAnswer", question.isMultiAnswer())
                .addValue("text", question.getText())
                .addValue("points", question.getPoints())
                .addValue("testId", question.getTestId());
        namedParameterJdbcTemplate.update("INSERT INTO `database`.question (isMultiAnswer, text, points, testId) VALUES (:isMultiAnswer, :text, :points, :testId)", sqlParameterSource, keyHolder);
        return keyHolder.getKey().longValue();
    }

    @Override
    public void update(Question question) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("UPDATE `database`.question SET isMultiAnswer = ?, text = ?, points = ?, testId = ? WHERE id = ?",
                new Object[]{question.isMultiAnswer(), question.getText(), question.getPoints(), question.getTestId(), question.getId()});
    }

    @Override
    public void remove(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM `database`.question WHERE id = ?", id);
    }

    @Override
    public Question get(long id) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.queryForObject("SELECT * FROM `database`.question WHERE id = ?", new Object[]{id}, new QuestionMapper());
    }

    @Override
    public List<Question> getQuestionsByTestId(long testId) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.question WHERE test_id = ?", new Object[]{testId},
                new QuestionMapper());
    }

    @Override
    public List<Question> getAllQuestions() {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        return jdbcTemplate.query("SELECT * FROM `database`.question", new QuestionMapper());
    }
}
