package com.antropovp.tp.dao;

import com.antropovp.tp.dto.User;

import java.util.List;

public interface UserDao {

    long add(User user);
    void update(User user);
    void remove(long id);
    void remove(String username);

    User get(long id);
    User get(String username);

    List<User> getAllUsers();
    List<User> getAllTeachers();
    List<User> getAllStudents();
}
