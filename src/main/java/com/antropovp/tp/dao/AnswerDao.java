package com.antropovp.tp.dao;

import com.antropovp.tp.dto.Answer;

import java.util.List;

public interface AnswerDao {

    long add(Answer answer);
    void update(Answer answer);
    void remove(long id);

    Answer get(long id);
    List<Answer> getAnswersByQuestionId(long questionId);
    List<Answer> getAllAnswers();
}
