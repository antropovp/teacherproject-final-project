package com.antropovp.tp.controller;

import com.antropovp.tp.dto.Question;
import com.antropovp.tp.dto.Test;
import com.antropovp.tp.service.QuestionService;
import com.antropovp.tp.service.SessionUserManager;
import com.antropovp.tp.service.TestCreatingManager;
import com.antropovp.tp.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class TestsController {

    //TODO: @PostMapping

    @Autowired
    private SessionUserManager sessionUserManager;

    @Autowired
    private TestCreatingManager testCreatingManager;

    @Autowired
    private TestService testService;

    @GetMapping("pages/tests")
    private ModelAndView tests(ModelAndView modelAndView) {
        modelAndView.setViewName("pages/tests");
        return modelAndView;
    }

    @PostMapping("create")
    private ModelAndView createTest(ModelAndView modelAndView, Test test) {
        test.setTeacherId(sessionUserManager.getCurrentSessionUser().getId());
        testService.create(test);
        testCreatingManager.setCurrentSessionTest(test);
        modelAndView.addObject("currentTest", test);
        modelAndView.setViewName("redirect:");
        return modelAndView;
    }

    @GetMapping("pages/t/create")
    private ModelAndView testCreating(ModelAndView modelAndView) {
        modelAndView.setViewName("pages/t/create");
        return modelAndView;
    }

    //?????
//    @PostMapping("/pages/t/create")
//    private ModelAndView createTest(ModelAndView modelAndView, Question question) {
//
//        modelAndView.setViewName("pages/tests");
//        return modelAndView;
//    }

    @GetMapping("/pages/t/change")
    private ModelAndView changeTest(ModelAndView modelAndView) {
        modelAndView.setViewName("/pages/t/change");
        return modelAndView;
    }

    @GetMapping("/pages/t/testing")
    private ModelAndView testing(ModelAndView modelAndView) {
        modelAndView.setViewName("/pages/t/testing");
        return modelAndView;
    }

}
