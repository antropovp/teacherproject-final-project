package com.antropovp.tp.controller;

import com.antropovp.tp.dto.User;
import com.antropovp.tp.service.SessionUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class InterfaceController {

    @Autowired
    private SessionUserManager sessionUserManager;

    @GetMapping("/")
    private ModelAndView interfaceView(ModelAndView modelAndView) {
        User currentUser = sessionUserManager.getCurrentSessionUser();

        modelAndView.addObject(currentUser);

        modelAndView.setViewName("interface");
        return modelAndView;
    }

    @GetMapping("pages/profile")
    private ModelAndView profile(ModelAndView modelAndView) {
        modelAndView.setViewName("pages/profile");
        return modelAndView;
    }

    @GetMapping("pages/users")
    private ModelAndView users(ModelAndView modelAndView) {
        modelAndView.setViewName("pages/users");
        return modelAndView;
    }
}