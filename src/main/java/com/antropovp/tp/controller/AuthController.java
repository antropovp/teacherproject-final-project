package com.antropovp.tp.controller;

import com.antropovp.tp.dto.User;
import com.antropovp.tp.exception.UnauthorizedException;
import com.antropovp.tp.exception.UserAlreadyExistsException;
import com.antropovp.tp.service.SessionUserManager;
import com.antropovp.tp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class AuthController {

    @Autowired
    private SessionUserManager sessionUserManager;

    @Autowired
    private UserService userService;

    @GetMapping("registration")
    public ModelAndView registration(ModelAndView modelAndView) {
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @PostMapping("registration")
    public ModelAndView registration(ModelAndView modelAndView, @Validated User user, BindingResult result) {
        modelAndView.setViewName("registration");

        if (result.hasErrors()) {
            List<ObjectError> errors = result.getAllErrors();
            modelAndView.addObject("errors", errors);
            return modelAndView;
        }

        try {
            userService.create(user);
        } catch (UserAlreadyExistsException e) {
            modelAndView.addObject("errors", "The user already exists.");
            return modelAndView;
        }

        sessionUserManager.setCurrentSessionUser(user);
        modelAndView.addObject("currentUser", user);
        modelAndView.setViewName("redirect:");
        return modelAndView;
    }

    @GetMapping("restore_password")
    public ModelAndView restorePassword(ModelAndView modelAndView) {
        modelAndView.setViewName("restore_password");
        return modelAndView;
    }

    //TODO: @PostMapping("restore_password")

    @ResponseBody
    @GetMapping("checkusernameexist")
    public Boolean username(@RequestParam String username) {
        return userService.checkUsernameExist(username);
    }

    @GetMapping("login")
    public ModelAndView login(ModelAndView modelAndView) {
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @PostMapping("login")
    public ModelAndView login(ModelAndView modelAndView, User user) {
        User foundUser;

        try {
            foundUser = userService.authenticate(user);
        } catch (UnauthorizedException e) {
            modelAndView.addObject("error", "Wrong username or password.");
            modelAndView.setViewName("login");
            return modelAndView;
        }

        sessionUserManager.setCurrentSessionUser(foundUser);
        modelAndView.setViewName("redirect:");

        return modelAndView;
    }

    @GetMapping("logout")
    public ModelAndView logout(ModelAndView modelAndView, HttpServletRequest request) {
        request.getSession().invalidate();
        modelAndView.setViewName("redirect:/login");
        return modelAndView;
    }
}
