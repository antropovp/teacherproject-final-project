package com.antropovp.tp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * Система Тестирование. Тьютор создает Тест из нескольких Вопросов закрытого типа (выбор одного
 или более вариантов из N предложенных) по определенному Предмету. Студент просматривает список
 доступных Тестов, отвечает на Вопросы.
 * @author Pavel Antropov
 */
@SpringBootApplication
public class TeacherProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeacherProjectApplication.class, args);
    }
}