package com.antropovp.tp.service;

import com.antropovp.tp.dto.Answer;

import java.util.List;

public interface AnswerService {

    long create(Answer answer);
    void delete(long id);

    Answer get(long id);
    List<Answer> getAllAnswers();
}
