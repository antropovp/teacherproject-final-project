package com.antropovp.tp.service;

import com.antropovp.tp.dto.Answer;
import com.antropovp.tp.dto.Question;

import java.util.List;

public interface QuestionService {

    long create(Question question);
    void delete(long id);

    Question get(long id);
    List<Question> getAllQuestions();

    //Answer
    void addAnswer(long questionId, Answer answer);
    void deleteAnswer(long questionId, long answerId);
    List<Answer> getAnswers(long questionId);
}
