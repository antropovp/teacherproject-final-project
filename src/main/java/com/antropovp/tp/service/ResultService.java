package com.antropovp.tp.service;

import com.antropovp.tp.dto.Result;

import java.util.List;

public interface ResultService {

    long create(Result result);
    void delete(long id);

    Result get(long id);
    List<Result> getAll();
}
