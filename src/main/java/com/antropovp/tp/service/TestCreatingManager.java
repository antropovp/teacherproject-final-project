package com.antropovp.tp.service;

import com.antropovp.tp.dto.Test;

public interface TestCreatingManager {

    Test setCurrentSessionTest(Test test);
    Test getCurrentSessionTest();
}
