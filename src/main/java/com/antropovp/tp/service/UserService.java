package com.antropovp.tp.service;

import com.antropovp.tp.dto.User;
import com.antropovp.tp.exception.UnauthorizedException;
import com.antropovp.tp.exception.UserAlreadyExistsException;

import java.util.List;

public interface UserService {

    long create(User user) throws UserAlreadyExistsException;
    void delete(long id);
    void delete(String username);

    User get(long id);
    User get(String username);

    List<User> getAllUsers();
    List<User> getAllTeachers();
    List<User> getAllStudents();

    User authenticate(User user) throws UnauthorizedException;
    boolean checkUsernameExist(String username);
}
