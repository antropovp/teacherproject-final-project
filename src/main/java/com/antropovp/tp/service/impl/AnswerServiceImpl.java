package com.antropovp.tp.service.impl;

import com.antropovp.tp.dao.AnswerDao;
import com.antropovp.tp.dto.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements com.antropovp.tp.service.AnswerService {

    @Autowired
    private AnswerDao answerDAO;

    @Override
    public List<Answer> getAllAnswers() {
        return answerDAO.getAllAnswers();
    }

    @Override
    public Answer get(long id) {
        return answerDAO.get(id);
    }

    @Override
    public long create(Answer answer) {
        return answerDAO.add(answer);
    }

    @Override
    public void delete(long id) {
        answerDAO.remove(id);
    }
}
