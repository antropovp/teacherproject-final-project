package com.antropovp.tp.service.impl;

import com.antropovp.tp.dao.AnswerDao;
import com.antropovp.tp.dao.QuestionDao;
import com.antropovp.tp.dto.Answer;
import com.antropovp.tp.dto.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements com.antropovp.tp.service.QuestionService {

    @Autowired
    private QuestionDao questionDAO;

    @Autowired
    private AnswerDao answerDAO;


    //Question

    @Override
    public List<Question> getAllQuestions() {
        return questionDAO.getAllQuestions();
    }

    @Override
    public Question get(long id) {
        return questionDAO.get(id);

    }

    @Override
    public long create(Question question) {
        return questionDAO.add(question);
    }

    @Override
    public void delete(long id) {
        questionDAO.remove(id);
    }


    //Answer

    @Override
    public void addAnswer(long questionId, Answer answer) {
        Question question = questionDAO.get(questionId);
        List<Answer> answers = question.getAnswers();
        answers.add(answer);
        question.setAnswers(answers);
        questionDAO.update(question);
    }

    @Override
    public void deleteAnswer(long questionId, long answerId) {
        Question question = questionDAO.get(questionId);
        List<Answer> answers = question.getAnswers();
        answers.remove(answerDAO.get(answerId));
        answerDAO.remove(answerId);
        question.setAnswers(answers);
        questionDAO.update(question);
    }

    @Override
    public List<Answer> getAnswers(long questionId) {
        return answerDAO.getAnswersByQuestionId(questionId);
    }
}
