package com.antropovp.tp.service.impl;

import com.antropovp.tp.dto.User;
import com.antropovp.tp.service.SessionUserManager;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

@Service
@SessionScope(proxyMode = ScopedProxyMode.INTERFACES)
public class SessionUserManagerImpl implements SessionUserManager {

    private User currentSessionUser;

    @Override
    public User setCurrentSessionUser(User user) {
        this.currentSessionUser = user;
        return currentSessionUser;
    }

    @Override
    public User getCurrentSessionUser() {
        return this.currentSessionUser;
    }
}
