package com.antropovp.tp.service.impl;

import com.antropovp.tp.dto.Test;
import com.antropovp.tp.service.TestCreatingManager;
import org.springframework.stereotype.Service;

@Service
public class TestCreatingManagerImpl implements TestCreatingManager {

    private Test currentSessionTest;

    @Override
    public Test setCurrentSessionTest(Test test) {
        this.currentSessionTest = test;
        return currentSessionTest;
    }

    @Override
    public Test getCurrentSessionTest() {
        return this.currentSessionTest;
    }
}
