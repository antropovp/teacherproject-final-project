package com.antropovp.tp.service.impl;

import com.antropovp.tp.dao.ResultDao;
import com.antropovp.tp.dto.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultServiceImpl implements com.antropovp.tp.service.ResultService {

    @Autowired
    ResultDao resultDao;

    @Override
    public List<Result> getAll() {
        return resultDao.getAllResults();
    }

    @Override
    public Result get(long id) {
        return resultDao.get(id);
    }

    @Override
    public long create(Result result) {
        return resultDao.add(result);
    }

    @Override
    public void delete(long id) {
        resultDao.remove(id);
    }

}
