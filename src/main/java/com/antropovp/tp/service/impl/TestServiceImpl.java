package com.antropovp.tp.service.impl;

import com.antropovp.tp.dao.QuestionDao;
import com.antropovp.tp.dao.TestDao;
import com.antropovp.tp.dto.Question;
import com.antropovp.tp.dto.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements com.antropovp.tp.service.TestService {

    @Autowired
    private TestDao testDao;

    @Autowired
    private QuestionDao questionDao;


    //Test

    @Override
    public long create(Test test) {
        return testDao.add(test);
    }

    @Override
    public void delete(long id) {
        testDao.remove(id);
    }

    @Override
    public Test get(long id) {
        return testDao.get(id);
    }

    @Override
    public List<Test> getAllTests() {
        return testDao.getAllTests();
    }


    //Question

    @Override
    public boolean addQuestion(long testId, long questionId) {
        if (testDao.get(testId) != null) {
            List<Question> questions = testDao.get(testId).getQuestions();
            questions.add(questionDao.get(questionId));
            testDao.get(testId).setQuestions(questions);
        }
        return false;
    }

    @Override
    public boolean deleteQuestion(long testId, long questionId) {
        if (testDao.get(testId) != null) {
            List<Question> questions = testDao.get(testId).getQuestions();
            for (int i = 0; i < questions.size(); i++) {
                if (questions.get(i).getId() == questionId) {
                    questions.remove(i);
                    testDao.get(testId).setQuestions(questions);
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public List<Question> getQuestions(long testId) {
        return questionDao.getQuestionsByTestId(testId);
    }
}
