package com.antropovp.tp.service.impl;

import com.antropovp.tp.dao.ResultDao;
import com.antropovp.tp.dao.TestDao;
import com.antropovp.tp.dto.Answer;
import com.antropovp.tp.dto.Question;
import com.antropovp.tp.dto.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestingServiceImpl implements com.antropovp.tp.service.TestingService {

    @Autowired
    private TestDao testDao;

    @Autowired
    private ResultDao resultDao;

    @Override
    public int checkAnswer(Question question, Answer answer) {
        if (answer.isRight() == true) {
            return question.getPoints();
        }
        return 0;
    }
    @Override
    public int checkAnswer(Question question, List<Answer> answers) {
        if (answers.size() > 0) {
            for (Answer check : answers) {
                if (check.isRight() == false) {
                    return 0;
                }
            }
        }
        return question.getPoints();
    }

    @Override
    public void nextQuestion() {

    }

    @Override
    public void previousQuestion() {

    }
}
