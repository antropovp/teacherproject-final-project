package com.antropovp.tp.service.impl;

import com.antropovp.tp.dao.UserDao;
import com.antropovp.tp.dto.User;
import com.antropovp.tp.exception.UnauthorizedException;
import com.antropovp.tp.exception.UserAlreadyExistsException;
import com.antropovp.tp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserDao userDao;

    @Override
    public long create(User user) throws UserAlreadyExistsException {
        try {
            get(user.getUsername());
            throw new UserAlreadyExistsException("Username is taken");
        } catch (DataAccessException e) {
            return userDao.add(user);
        }
    }

    @Override
    public void delete(long id) {
        userDao.remove(id);
    }

    @Override
    public void delete(String username) {
        userDao.remove(username);
    }

    @Override
    public User get(long id) {
        return userDao.get(id);
    }

    @Override
    public User get(String username) {
        return userDao.get(username);
    }


    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public List<User> getAllTeachers() {
        return userDao.getAllTeachers();
    }

    @Override
    public List<User> getAllStudents() {
        return userDao.getAllStudents();
    }


    @Override
    public User authenticate(User user) throws UnauthorizedException {
        try {
            User foundUser = userDao.get(user.getUsername());
            if (!foundUser.getPassword().equals(user.getPassword())) {
                throw new UnauthorizedException("User is not authorized.");
            }
            return foundUser;
        } catch (DataAccessException e) {
            throw new UnauthorizedException("User is not authorized.");
        }
    }

    @Override
    public boolean checkUsernameExist(String username) {
        return Objects.isNull(userDao.get(username));
    }
}
