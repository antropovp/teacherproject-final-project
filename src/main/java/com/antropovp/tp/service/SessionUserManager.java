package com.antropovp.tp.service;

import com.antropovp.tp.dto.User;

public interface SessionUserManager {

    User setCurrentSessionUser(User user);
    User getCurrentSessionUser();

}
