package com.antropovp.tp.service;

public interface GradingService {

    char getGrade();

}
