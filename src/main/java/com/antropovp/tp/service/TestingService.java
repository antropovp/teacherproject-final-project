package com.antropovp.tp.service;

import com.antropovp.tp.dto.Answer;
import com.antropovp.tp.dto.Question;

import java.util.List;

public interface TestingService {

    int checkAnswer(Question question, Answer answer); //баллы за ответ - будут прибавляться к результату в Result
    int checkAnswer(Question question, List<Answer> answers); //для ответов с несколькими вариантами
    void nextQuestion();
    void previousQuestion();

}
