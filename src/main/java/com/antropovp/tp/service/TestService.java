package com.antropovp.tp.service;

import com.antropovp.tp.dto.Question;
import com.antropovp.tp.dto.Test;

import java.util.List;

public interface TestService {

    long create(Test test);
    void delete(long id);

    Test get(long id);
    List<Test> getAllTests();

    //Question
    boolean addQuestion(long testId, long questionId);
    boolean deleteQuestion(long testId, long questionId);
    List<Question> getQuestions(long testId);
}
